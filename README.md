# qa_project

Browserstack + Python Selenium

- There are 5 test cases written in Selenium (Python) and all of them are connected to Browserstack
- In each Python file there are 5 configurations that run parallel on Browserstack (2 on windows, 1 on OS X, one on Android and one on iOS)
- Also, this Git repository is connected to GitLab CI/CD so that whole taest suit can be orchestrated remotely
